package com.viettel.qlan.dao;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import com.viettel.qlan.bo.Objects;
import com.viettel.qlan.bo.RoleObject;
import com.viettel.qlan.dto.RoleObjectDTO;
import com.viettel.service.base.dao.BaseFWDAOImpl;

@Repository("roleObjectDAO")
public class RoleObjectDAO extends BaseFWDAOImpl<RoleObject, Long> {
	public RoleObjectDAO() {
		this.model= new RoleObject();
	}
	public RoleObjectDAO(Session session ) {
		this.session = session;
	}
	
	public void DeleteThenInsertRole(RoleObjectDTO obj) {
		System.out.println(obj.getObjectIdList().toArray().length);
		if(obj.getObjectIdList()==null) {
			return;
		}
		StringBuilder sql1= new StringBuilder("delete from role_object where role_id=:roleId");
		SQLQuery queryDelete=getSession().createSQLQuery(sql1.toString());
		queryDelete.setParameter("roleId", obj.getRoleId());
		queryDelete.executeUpdate();
//		StringBuilder sql= new StringBuilder(""
//				+ "insert into role_object(object_id,role_id,is_active) "
//				+ "values(:objectId,:roleId,1)");
//		SQLQuery queryDelete=getSession().createSQLQuery(sql1.toString());
//		queryDelete.executeUpdate();
//		SQLQuery query=getSession().createSQLQuery(sql.toString());
//		
//		query.setParameter("objectId", obj.getObjectId());
//		query.setParameterList("objectIdList", obj.getObjectListId());
//		
//		query.executeUpdate();

		for(Long ListId:obj.getObjectIdList()) {
			StringBuilder sql= new StringBuilder(""
					+ "insert into role_object(object_id,role_id,is_active) "
					+ "values(:objectId,:roleId,'1')");
			SQLQuery query=getSession().createSQLQuery(sql.toString());
			query.setParameter("objectId", ListId);
			query.setParameter("roleId", obj.getRoleId());
			query.executeUpdate();
		}
	}

}
