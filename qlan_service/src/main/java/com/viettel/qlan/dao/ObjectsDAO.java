package com.viettel.qlan.dao;

import java.math.BigInteger;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.viettel.qlan.bo.Objects;
import com.viettel.qlan.dto.ObjectsDTO;
import com.viettel.qlan.dto.RoleObjectDTO;
import com.viettel.qlan.utils.ValidateUtils;
import com.viettel.service.base.dao.BaseFWDAOImpl;
@Repository("objectsDAO")
public class ObjectsDAO extends BaseFWDAOImpl<Objects, Long> {
	public ObjectsDAO() {
		this.model = new Objects();
	}

	public ObjectsDAO(Session session) {
		this.session = session;
	}

	
	
	@SuppressWarnings("unchecked")
	public List<ObjectsDTO> getListObjects(Long userId,Long parentId){
		StringBuilder sql= new StringBuilder("SELECT DISTINCT ");
		sql.append(" obj.OBJECT_ID AS objectId,");
		sql.append(" obj.PARENT_ID AS parentId,");
		sql.append(" obj.`STATUS` AS `status`,");
		sql.append(" obj.ORD AS ord,");
		sql.append(" obj.OBJECT_URL AS objectUrl,");
		sql.append(" obj.OBJECT_NAME AS objectName,");
		sql.append(" obj.DESCRIPTION AS description,");
		sql.append(" obj.OBJECT_TYPE_ID AS objectTypeId,");
		sql.append(" obj.OBJECT_CODE AS objectCode,");
		sql.append(" obj.CREATE_USER AS createUser,");
		sql.append(" obj.CREATE_DATE AS createDate");
		sql.append(" FROM objects AS obj  ");
		
		if(parentId !=null){
			sql.append(" JOIN role_object on obj.OBJECT_ID=role_object.OBJECT_ID");
			sql.append(" JOIN role_user ON role_user.ROLE_ID=role_object.ROLE_ID");
			sql.append(" WHERE role_user.USER_ID=:userId AND role_user.IS_ACTIVE=1 AND role_object.IS_ACTIVE=1 AND obj.`STATUS`=1 ");
			sql.append(" AND obj.PARENT_ID =:parentId ");
		} else {
			sql.append(" WHERE obj.OBJECT_ID IN (SELECT objects.PARENT_ID FROM  objects ");
			sql.append(" JOIN role_object on objects.OBJECT_ID=role_object.OBJECT_ID");
			sql.append(" JOIN role_user ON role_user.ROLE_ID=role_object.ROLE_ID");
			sql.append(" WHERE role_user.USER_ID=:userId AND role_user.IS_ACTIVE=1 AND role_object.IS_ACTIVE=1  ) AND obj.`STATUS`=1 ");
		}
		sql.append(" ORDER BY obj.ORD ");
		
		SQLQuery query= getSession().createSQLQuery(sql.toString());
		
		query.addScalar("objectId", new LongType());
		query.addScalar("parentId", new LongType());
		query.addScalar("status", new LongType());
		query.addScalar("ord", new LongType());
		query.addScalar("objectUrl", new StringType());
		query.addScalar("objectName", new StringType());
		query.addScalar("description", new StringType());
		query.addScalar("objectTypeId", new LongType());
		query.addScalar("objectCode", new StringType());
		query.addScalar("createUser", new StringType());
		query.addScalar("createDate", new DateType());
		
		query.setResultTransformer(Transformers.aliasToBean(ObjectsDTO.class));
		
		query.setParameter("userId", userId);
		if(parentId!=null){
			query.setParameter("parentId", parentId);
		}
		
		return query.list();
	}
	public List<ObjectsDTO> autoSearchObject(ObjectsDTO obj){
		String sql = "select o.object_id objectId,o.object_code objectCode," + 
				"o.object_name objectName," + 
				"o.object_url objectUrl," + 
				
				"o.ord ord, " +
				"o.object_type_id objectTypeId"
				+ " from objects o "
				+ "WHERE 1=1  ";
		if(obj.getListId().size()>0){
			sql+=" and o.object_ID NOT IN (:listId)";
		}
		
		StringBuilder stringBuilder = new StringBuilder(sql);
		
			if(StringUtils.isNotEmpty(obj.getObjectName())){
			stringBuilder.append(" AND (upper(o.object_name) LIKE upper(:name) escape '&' OR upper(o.object_code) LIKE upper(:name) escape '&')");
			}
		
		stringBuilder.append(" ORDER BY o.object_CODE");
		stringBuilder.append(" LIMIT 10 ");
		
		SQLQuery query = getSession().createSQLQuery(stringBuilder.toString());
		query.addScalar("objectId", new LongType());
		query.addScalar("objectCode", new StringType());
		query.addScalar("objectName", new StringType());
		query.addScalar("objectUrl", new StringType());
		query.addScalar("ord", new LongType());
		query.addScalar("objectTypeId", new LongType());
	
		query.setResultTransformer(Transformers.aliasToBean(ObjectsDTO.class));

		if (StringUtils.isNotEmpty(obj.getObjectName())) {
			query.setParameter("name", "%" + ValidateUtils.validateKeySearch(obj.getObjectName()) + "%");
		}
		
		if(obj.getListId().size()>0) {
			query.setParameterList("listId", obj.getListId());
		}
//		query.setParameter("objectTypeId",1L);
		return query.list();

	}
}
