package com.viettel.qlan.dao;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.transform.Transformers;
import org.hibernate.type.DateType;
import org.hibernate.type.LongType;
import org.hibernate.type.StringType;
import org.springframework.stereotype.Repository;

import com.viettel.qlan.bo.Roles;
import com.viettel.qlan.dto.RolesDTO;
import com.viettel.qlan.dto.UsersDTO;
import com.viettel.qlan.dto.ObjectsDTO;
import com.viettel.qlan.dto.RoleObjectDTO;
import com.viettel.qlan.dto.RoleUserDTO;
import com.viettel.qlan.utils.ValidateUtils;
import com.viettel.service.base.dao.BaseFWDAOImpl;

@Repository("rolesDAO")
public class RolesDAO extends BaseFWDAOImpl<Roles, Long> {
	public RolesDAO() {
		this.model = new Roles();
	}

	public RolesDAO(Session session) {
		this.session = session;
	}

	@SuppressWarnings("unchecked")
	public List<RolesDTO> getForAutoCompleteRoles(RolesDTO obj) {
		String sql = "select o.ROLE_ID AS roleId," + "o.`STATUS` status," + "o.ROLE_NAME roleName," +

				"o.ROLE_CODE roleCode, " + "o.DESCRIPTION  description " + " from roles o where 1=1";

		StringBuilder stringBuilder = new StringBuilder(sql);

		if (obj.getStatus() != null) {
			stringBuilder.append(" and o.`STATUS` = :status");

		}
		if (StringUtils.isNotEmpty(obj.getRoleCode())) {
			stringBuilder.append(
					"  AND (upper(o.role_code) LIKE upper(:code)  escape '&' OR upper(o.ROLE_name) LIKE upper(:code) escape '&')");
		}

		if (StringUtils.isNotEmpty(obj.getRoleName())) {
			stringBuilder.append(
					" AND (upper(o.ROLE_NAME) LIKE upper(:name) escape '&' OR upper(o.ROLE_CODE) LIKE upper(:name) escape '&')");
		}
		if (obj.getStatus() != null) {
			stringBuilder.append(" and o.`STATUS` = :status");

		}
		if (StringUtils.isNotEmpty(obj.getDescription())) {
			stringBuilder.append("  AND upper(o.description) LIKE upper(:description)  escape '&' ");
		}

		stringBuilder.append(" ORDER BY o.ROLE_CODE");
		// stringBuilder.append(" LIMIT 10 ");
		StringBuilder sqlCount = new StringBuilder("SELECT COUNT(*) FROM (");
		sqlCount.append(stringBuilder.toString());
		sqlCount.append(")");
		sqlCount.append(" as objects;");

		SQLQuery query = getSession().createSQLQuery(stringBuilder.toString());
		SQLQuery queryCount = getSession().createSQLQuery(sqlCount.toString());

		query.addScalar("description", new StringType());
		query.addScalar("roleName", new StringType());
		query.addScalar("roleCode", new StringType());
		query.addScalar("roleId", new LongType());
		query.addScalar("status", new LongType());

		query.setResultTransformer(Transformers.aliasToBean(RolesDTO.class));

		if (obj.getStatus() != null) {
			query.setParameter("status", +obj.getStatus());
			queryCount.setParameter("status", obj.getStatus());
		}
		if (StringUtils.isNotEmpty(obj.getRoleName())) {
			query.setParameter("name", "%" + ValidateUtils.validateKeySearch(obj.getRoleName()) + "%");
			queryCount.setParameter("name", "%" + ValidateUtils.validateKeySearch(obj.getRoleName()) + "%");
		}

		if (StringUtils.isNotEmpty(obj.getRoleCode())) {
			query.setParameter("code", "%" + ValidateUtils.validateKeySearch(obj.getRoleCode()) + "%");
			queryCount.setParameter("code", "%" + ValidateUtils.validateKeySearch(obj.getRoleCode()) + "%");
		}
		if (StringUtils.isNotEmpty(obj.getDescription())) {
			query.setParameter("description", "%" + ValidateUtils.validateKeySearch(obj.getDescription()) + "%");
			queryCount.setParameter("description", "%" + ValidateUtils.validateKeySearch(obj.getDescription()) + "%");
		}
		if (obj.getListId().size() > 0) {
			query.setParameterList("listId", obj.getListId());
		}
		// query.setParameter("status",1L);

		query.setFirstResult((obj.getPage().intValue() - 1) * obj.getPageSize().intValue());
		query.setMaxResults(obj.getPageSize().intValue());
		obj.setTotalRecord(((BigInteger) queryCount.uniqueResult()).intValue());

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<RoleUserDTO> getListRoleByUserId(Long userId) {
		String sql = "select o.ROLE_ID AS roleId," + "o.`STATUS` status," + "o.ROLE_NAME roleName,"
				+ "o.ROLE_CODE roleCode, " + "o.DESCRIPTION  description," + "ru.USER_ID  userId" + " from roles o "
				+ " JOIN role_user ru ON ru.ROLE_ID=o.ROLE_ID " + " WHERE o.status=1 AND  ru.USER_ID=:userId ";

		StringBuilder stringBuilder = new StringBuilder(sql);

		stringBuilder.append(" ORDER BY o.ROLE_CODE");

		SQLQuery query = getSession().createSQLQuery(stringBuilder.toString());
		query.addScalar("description", new StringType());
		query.addScalar("roleName", new StringType());
		query.addScalar("roleCode", new StringType());
		query.addScalar("roleId", new LongType());
		query.addScalar("status", new LongType());
		query.addScalar("userId", new LongType());

		query.setResultTransformer(Transformers.aliasToBean(RoleUserDTO.class));
		query.setLong("userId", userId);
		return query.list();
	}

	public RolesDTO getRolesInfo(String roleName) {
		StringBuilder sql = new StringBuilder("Select " + "roles.role_id as roleId ," + "roles.status as status ,"
				+ "roles.role_name as roleName ," + "roles.description as description, "
				+ "roles.role_code as roleCode," + "roles.create_date as createDate,"
				+ "roles.create_user as createUser " + "from roles " + "Where upper(roles.role_name) = upper(:roleName)"

		);
		SQLQuery query = getSession().createSQLQuery(sql.toString());

		query.addScalar("roleId", new LongType());
		query.addScalar("status", new LongType());
		query.addScalar("roleName", new StringType());
		query.addScalar("description", new StringType());
		query.addScalar("roleCode", new StringType());
		query.addScalar("createDate", new DateType());
		query.addScalar("createUser", new StringType());

		query.setResultTransformer(Transformers.aliasToBean(RolesDTO.class));

		query.setParameter("roleId", roleName);

		return (RolesDTO) query.uniqueResult();

	}

	public void lockRole(Long roleId) {
		StringBuilder sql = new StringBuilder("Update roles set status =0 where role_id=:roleId");

		SQLQuery query = getSession().createSQLQuery(sql.toString());

		query.setParameter("roleId", roleId);
		query.executeUpdate();
	}

	public void unlockRole(Long roleId) {
		StringBuilder sql = new StringBuilder("Update roles set status =1 where role_id=:roleId");

		SQLQuery query = getSession().createSQLQuery(sql.toString());

		query.setParameter("roleId", roleId);
		query.executeUpdate();
	}

	public Integer deleteRole(String roleId) {
		StringBuilder sql = new StringBuilder("delete from roles where role_id = :roleId");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.setParameter("roleId", roleId);

		StringBuilder sqlCheckRoleObject = new StringBuilder("select role_id from role_object where role_id=:roleId");
		SQLQuery queryCheck = getSession().createSQLQuery(sqlCheckRoleObject.toString());
		queryCheck.setParameter("roleId", roleId);
		try {
			if (queryCheck.uniqueResult() != null) {
				return 0;
			}
		} catch (Exception e) {
			return 0;
		}

		query.executeUpdate();
		System.out.println(sql);
		return 1;
	}

	public int addRole(RolesDTO obj, HttpServletRequest request) {
		// todo
		StringBuilder sqlCheck = new StringBuilder("Select role_code from roles where role_code ='");
		sqlCheck.append(obj.getRoleCode());
		sqlCheck.append("'");
		SQLQuery queryCheck = getSession().createSQLQuery(sqlCheck.toString());

		queryCheck.uniqueResult();
		if (queryCheck.uniqueResult() == null) {
			
		} else {
			System.out.println("in else");
			return 0;
		}

		StringBuilder sql = new StringBuilder(
				"insert into roles(status,role_name,description,role_code,create_date,create_user)"
						+ " values (:status,:roleName,:description,:roleCode,:createDate,:createUser)");
		HttpSession httpSession = request.getSession();
		UsersDTO userDto = (UsersDTO) httpSession.getAttribute("userInfo");

		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
		LocalDateTime now = LocalDateTime.now();
		System.out.println(dtf.format(now));

		SQLQuery query = getSession().createSQLQuery((sql.toString()));
		query.setParameter("status", obj.getStatus());
		query.setParameter("roleName", obj.getRoleName());
		query.setParameter("description", obj.getDescription());
		query.setParameter("roleCode", obj.getRoleCode());
		query.setParameter("createDate", dtf.format(now));
		query.setParameter("createUser", obj.getCreateUser());

		query.executeUpdate();

		return 1;
	}

	public void updateRole(RolesDTO obj) {
		System.out.println("in rolesDAO");
		System.out.println(obj);
		StringBuilder sql = new StringBuilder("" + "update roles set status=:status,role_name=:roleName,description="
				+ "'" + obj.getDescription() + "'" + "where role_id=:roleId");
		SQLQuery query = getSession().createSQLQuery(sql.toString());
		query.setParameter("status", obj.getStatus());
		query.setParameter("roleName", obj.getRoleName());
		query.setParameter("roleId", obj.getRoleId());
		System.out.println(sql);
		query.executeUpdate();
	}

	public List<ObjectsDTO> getObjectsByRolesId(RolesDTO obj) {
		StringBuilder stringBuilder = new StringBuilder(
				"select o.object_id objectId, o.object_code objectCode,o.object_name objectName,o.object_url objectUrl,o.ord ord,o.object_type_id objectTypeId \r\n"
						+ "from objects o where o.object_id in (select ro.object_id from role_object ro where ro.role_id = :roleId )\r\n"
						+ "group by o.OBJECT_id");

		SQLQuery query = getSession().createSQLQuery(stringBuilder.toString());
		SQLQuery queryCount = getSession().createSQLQuery(stringBuilder.toString());

		query.setParameter("roleId", obj.getRoleId());

		query.addScalar("objectId", new LongType());
		query.addScalar("objectCode", new StringType());
		query.addScalar("objectName", new StringType());
		query.addScalar("objectUrl", new StringType());
		query.addScalar("ord", new LongType());
		query.addScalar("objectTypeId", new LongType());

		query.setResultTransformer(Transformers.aliasToBean(ObjectsDTO.class));

		query.setFirstResult((obj.getPage().intValue() - 1) * obj.getPageSize().intValue());
		query.setMaxResults(obj.getPageSize().intValue());
		obj.setTotalRecord((query.list().size()));

		return query.list();
	}

}
