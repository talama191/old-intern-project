package com.viettel.qlan.business;

import java.util.List;

import com.viettel.qlan.dao.RolesDAO;
import com.viettel.qlan.dto.ObjectsDTO;
import com.viettel.qlan.dto.RolesDTO;
import com.viettel.service.base.dto.DataListDTO;

public interface RolesBusiness {

	DataListDTO doSearch(RolesDTO obj);

	DataListDTO getObjectsByRoleId(RolesDTO obj);

	List<ObjectsDTO> autoSearchObject(ObjectsDTO obj);

	
}
