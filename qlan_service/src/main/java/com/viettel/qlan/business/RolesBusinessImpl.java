package com.viettel.qlan.business;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.viettel.base.common.BusinessException;
import com.viettel.qlan.bo.Roles;
import com.viettel.qlan.constant.Constants;
import com.viettel.qlan.dao.ActionAuditDAO;
import com.viettel.qlan.dao.AuditDetailDAO;
import com.viettel.qlan.dao.ObjectsDAO;
import com.viettel.qlan.dao.RoleObjectDAO;
import com.viettel.qlan.dao.RolesDAO;
import com.viettel.qlan.dto.ObjectsDTO;
import com.viettel.qlan.dto.ResultDTO;
import com.viettel.qlan.dto.RoleObjectDTO;
import com.viettel.qlan.dto.RolesDTO;
import com.viettel.qlan.dto.RolesDTO;
import com.viettel.qlan.utils.QlanResource;
import com.viettel.service.base.business.BaseFWBusinessImpl;
import com.viettel.service.base.dto.DataListDTO;

@Service("rolesBusinessImpl")
@Scope(proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RolesBusinessImpl extends BaseFWBusinessImpl<RolesDAO, RolesDTO, Roles> implements RolesBusiness {

	private static final Logger Log = Logger.getLogger(RolesBusinessImpl.class);

	private static String TABLE_NAME = "ROLES";

	@Autowired
	private RolesDAO rolesDAO;
	
	@Autowired
	private RoleObjectDAO roleObjectDao;
	
	@Autowired
	private ObjectsDAO objectDAO;

	@Autowired
	private ActionAuditDAO actionAuditDAO;

	@Autowired
	private AuditDetailDAO auditDetailDAO;

	@Autowired
	ActionAuditBusinessImpl actionAuditBusinessImpl;

	public RolesBusinessImpl() {
		tModel = new Roles();
		tDAO = rolesDAO;
	}

	@Override
	public DataListDTO doSearch(RolesDTO obj) {
		List<RolesDTO> list = rolesDAO.getForAutoCompleteRoles(obj);
		DataListDTO data = new DataListDTO();

		data.setData(list);
		data.setTotal(obj.getTotalRecord());
		data.setSize(obj.getTotalRecord());
		data.setStart(1);
		return data;
	};

	@Transactional
	public void Lock(List<RolesDTO> listRoles, HttpServletRequest request) throws Exception {

		for (RolesDTO obj : listRoles) {
			
			rolesDAO.lockRole(obj.getRoleId());
		}
	}

	@Transactional
	public void unLock(List<RolesDTO> listRoles, HttpServletRequest request) throws Exception {

		for (RolesDTO obj : listRoles) {
			
			rolesDAO.unlockRole(obj.getRoleId());
		}
	}

	@Transactional
	public Long deleteRoles(RolesDTO obj, HttpServletRequest request) throws Exception {

		HttpSession httpSession = request.getSession();
		
		int a=rolesDAO.deleteRole(obj.getRoleId().toString());
		if(a==0) {
			throw new BusinessException(QlanResource.get("exist_role_object_under_role"));
		}
		System.out.println("da delete");
		return obj.getRoleId();
	}

	@Transactional
	public Long DeleteThenInsertRole(RoleObjectDTO obj,HttpServletRequest request) {
		HttpSession httpSession=request.getSession();
		if(obj ==null) {
			return (long) 1;
		}
		roleObjectDao.DeleteThenInsertRole(obj);
		return (long) 1;
	}
	
	@Transactional
	public Long add(RolesDTO obj, HttpServletRequest request) throws Exception {
		HttpSession httpSession = request.getSession();
		ResultDTO resultDTO=new ResultDTO();
		
		RolesDTO roleDto = (RolesDTO) httpSession.getAttribute("roleInfo");
		if( rolesDAO.addRole(obj, request)==0) {
			throw new BusinessException(QlanResource.get("exist_role_code"));
		}
		Long id = (long) 1;

		return id;
	}

	@Transactional
	public void updateRoles(RolesDTO obj, HttpServletRequest request) throws Exception {
		System.out.println("in update");
		rolesDAO.updateRole(obj);

	}
	
	@Override
	public DataListDTO getObjectsByRoleId(RolesDTO obj) {
		List<ObjectsDTO> list=rolesDAO.getObjectsByRolesId(obj);
		DataListDTO data= new DataListDTO();
		data.setData(list);
		data.setTotal(obj.getTotalRecord());
		data.setSize(obj.getTotalRecord());
		data.setStart(1);
		return data;
	}
	@Override
	public List<ObjectsDTO> autoSearchObject(ObjectsDTO obj) {
		List<ObjectsDTO> list=objectDAO.autoSearchObject(obj);
		DataListDTO data= new DataListDTO();
		data.setData(list);
		data.setTotal(obj.getTotalRecord());
		data.setSize(obj.getTotalRecord());
		data.setStart(1);
		
		return list;
	}

}
