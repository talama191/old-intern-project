package com.viettel.qlan.rest;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;

import com.viettel.qlan.business.RolesBusinessImpl;
import com.viettel.qlan.dto.ObjectsDTO;
import com.viettel.qlan.dto.RoleObjectDTO;
import com.viettel.qlan.dto.RolesDTO;
import com.viettel.service.base.dto.DataListDTO;

public class RolesServiceImpl extends AbstractRsService implements RolesService {

	@Autowired
	RolesBusinessImpl rolesBusinessImpl;

	@Override
	public Response doSearch(RolesDTO obj) {
		DataListDTO data = rolesBusinessImpl.doSearch(obj);
		return Response.ok(data).build();
	}

	@Override
	public void lock(List<RolesDTO> listRoles) throws Exception {
		rolesBusinessImpl.Lock(listRoles, request);

	}

	@Override
	public void unlock(List<RolesDTO> listRoles) throws Exception {
		rolesBusinessImpl.unLock(listRoles, request);

	}

	@Override
	public Response remove(RolesDTO obj) throws Exception {
		if (obj != null) {

			try {
				Long id = rolesBusinessImpl.deleteRoles(obj, request);
				
				return Response.ok(id).build();
			} catch (Exception e) {
				return Response.ok().entity(Collections.singletonMap("error", e.getMessage())).build();
			}
			

		} else {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

	}

	@Override
	public Response add(RolesDTO obj) throws Exception {
		String roleCode = obj.getRoleCode();
		String roleName = obj.getRoleName();
		String description = obj.getDescription();
		Pattern patt = Pattern.compile("(?=^.{0,100}$)^[a-zA-Z0-9äöüÄÖÜ_]*$");
		Pattern patt2 = Pattern.compile("(?=^.{0,100}$)^[ a-zA-Z0-9äöüÄÖÜ_áàạảãâấầậẩẫăắằặẳẵÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴéèẹẻẽêếềệểễÉÈẸẺẼÊẾỀỆỂỄóòọỏõôốồộổỗơớờợởỡÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠúùụủũưứừựửữÚÙỤỦŨƯỨỪỰỬỮíìịỉĩÍÌỊỈĨđĐýỳỵỷỹÝỲỴỶỸ]*$");
		Pattern patt3 = Pattern.compile("(?=^.{0,1000}$)^[ a-zA-Z0-9äöüÄÖÜ_áàạảãâấầậẩẫăắằặẳẵÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴéèẹẻẽêếềệểễÉÈẸẺẼÊẾỀỆỂỄóòọỏõôốồộổỗơớờợởỡÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠúùụủũưứừựửữÚÙỤỦŨƯỨỪỰỬỮíìịỉĩÍÌỊỈĨđĐýỳỵỷỹÝỲỴỶỸ]*$");
		Matcher m1 = patt.matcher(roleCode);
		Matcher m2 = patt2.matcher(roleName);
		Matcher m3 = patt3.matcher(description);
		if (!m1.matches() || !m2.matches() || !m3.matches()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}

		try {
			if(rolesBusinessImpl.add(obj, request)==0) {
				return Response.status(Response.Status.BAD_REQUEST).build();
			}
			return Response.ok(1).build();
		} catch (Exception e) {
			
			return Response.ok().entity(Collections.singletonMap("error", e.getMessage())).build();
		}
	}

	@Override
	public Response update(RolesDTO obj) throws Exception {
		String roleCode = obj.getRoleCode();
		String roleName = obj.getRoleName();
		String description = obj.getDescription();
		Pattern patt = Pattern.compile("(?=^.{0,100}$)^[a-zA-Z0-9äöüÄÖÜ_]*$");
		Pattern patt2 = Pattern.compile("(?=^.{0,100}$)^[ a-zA-Z0-9äöüÄÖÜ_áàạảãâấầậẩẫăắằặẳẵÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴéèẹẻẽêếềệểễÉÈẸẺẼÊẾỀỆỂỄóòọỏõôốồộổỗơớờợởỡÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠúùụủũưứừựửữÚÙỤỦŨƯỨỪỰỬỮíìịỉĩÍÌỊỈĨđĐýỳỵỷỹÝỲỴỶỸ]*$");
		Pattern patt3 = Pattern.compile("(?=^.{0,1000}$)^[ a-zA-Z0-9äöüÄÖÜ_áàạảãâấầậẩẫăắằặẳẵÁÀẠẢÃÂẤẦẬẨẪĂẮẰẶẲẴéèẹẻẽêếềệểễÉÈẸẺẼÊẾỀỆỂỄóòọỏõôốồộổỗơớờợởỡÓÒỌỎÕÔỐỒỘỔỖƠỚỜỢỞỠúùụủũưứừựửữÚÙỤỦŨƯỨỪỰỬỮíìịỉĩÍÌỊỈĨđĐýỳỵỷỹÝỲỴỶỸ]*$");
		Matcher m1 = patt.matcher(roleCode);
		Matcher m2 = patt2.matcher(roleName);
		Matcher m3 = patt3.matcher(description);
		if (!m1.matches() || !m2.matches() || !m3.matches()) {
			return Response.status(Response.Status.BAD_REQUEST).build();
		}
		
		try {
			rolesBusinessImpl.updateRoles(obj, request);
			return Response.ok(1).build();
		} catch (Exception e) {
			return Response.ok().entity(Collections.singletonMap("error", e.getMessage())).build();
		}
	}

	@Override
	public Response getListObjectsByRoleId(RolesDTO obj) throws Exception {
		DataListDTO data = rolesBusinessImpl.getObjectsByRoleId(obj);
		System.out.println(data);
		return Response.ok(data).build();
	}

	@Override
	public Response autoSearchObject(ObjectsDTO obj) throws Exception {
		List<ObjectsDTO> data = rolesBusinessImpl.autoSearchObject(obj);

		return Response.ok(data).build();
	}

	@Override
	public Response insertObjectData(RoleObjectDTO obj) throws Exception {
		if (obj != null) {
			rolesBusinessImpl.DeleteThenInsertRole(obj, request);
			return Response.ok(1).build();
		}
		return null;
	}

}
