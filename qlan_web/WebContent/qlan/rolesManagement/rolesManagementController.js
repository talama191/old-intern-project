(function () {
  "use strict";
  var controllerId = "rolesManagementController";

  angular
    .module("MetronicApp")
    .controller(controllerId, rolesManagementController);

  function rolesManagementController(
    $scope,
    $rootScope,
    $timeout,
    gettextCatalog,
    kendoConfig,
    $kWindow,
    rolesManagementService,
    CommonService,
    PopupConst,
    Restangular,
    RestEndpoint,
    Constant
  ) {
    var vm = this;
    vm.showSearch = true;
    vm.isCreateNew = true;
    vm.showDetail = false;
    vm.checkcreate = false;
    vm.options = [0];
    vm.removeItem = removeItem;
    vm.rolesManagementSearch = {
      status: null,
      // objectTypeId: 2
    };
    vm.rolesManagementCreate = {
      status: 2,
      objectTypeId: 2,
    };
    vm.rolesManagement = {};
    vm.validatorOptions = kendoConfig.get("validatorOptions");
    initFormData();
    function initFormData() {
      fillDataTable([]);
      // getParent();
    }

    $scope.$watch("vm.rolesManagementCreate.checkIp", function () {
      if (vm.rolesManagementCreate.checkIp == 1) {
        $("#create_ip_lb").addClass("req");
        $("#create_ip").attr("required", true);
      } else {
        $("#create_ip_lb").removeClass("req");
        $("#create_ip").removeAttr("required");
      }
    });
    vm.commonSearch = { name: "", code: "" };

    vm.headerTemplate =
      '<div class="dropdown-header row text-center k-widget k-header">' +
      '<p class="col-md-6 text-header-auto border-right-ccc">Mã</p>' +
      '<p class="col-md-6 text-header-auto border-right-ccc">Tên</p>' +
      "</div>";
    vm.template =
      '<div class="row"><div class="col-xs-5" style="padding: 0px 32px 0 0,float:left">#: data.deptCode #</div>' +
      '<div style="padding-left: 10px,width:auto;overflow:hidden;text-align: right;">#: data.deptName #</div></div>';
    vm.status = [
      { id: 0, name: "Không hoạt động" },
      { id: 1, name: "Hoạt động" },
    ];
    vm.statusSelectOptions = {
      dataSource: vm.status,
      dataTextField: "name",
      dataValueField: "id",
      optionLabel: "---Chọn---",
      valuePrimitive: true,
    };
    vm.genderData = [
      { id: 0, name: "Nam" },
      { id: 1, name: "Nữ" },
      { id: 3, name: "Khác" },
    ];
    vm.genderSelectOptions = {
      dataSource: vm.genderData,
      dataTextField: "name",
      dataValueField: "id",
      optionLabel: "---Chọn---",
      valuePrimitive: true,
    };
    vm.checkIpData = [
      { id: 0, name: "Không" },
      { id: 1, name: "Có" },
    ];
    vm.checkIpSelectOptions = {
      dataSource: vm.checkIpData,
      dataTextField: "name",
      dataValueField: "id",
      optionLabel: "---Chọn---",
      valuePrimitive: true,
    };
    vm.optDept = {
      dataTextField: "deptName",

      valuePrimitive: true,
      optionLabel: "---Chọn---",
      dataSource: {
        serverFiltering: true,
        transport: {
          read: {
            type: "POST",
            url: RestEndpoint.BASE_SERVICE_URL + "commonServiceRest/getDept",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
          },
          parameterMap: function (options, operation) {
            return JSON.stringify({});
          },
        },
      },
    };

    vm.optDeptCreate = {
      dataTextField: "deptName",
      valuePrimitive: true,
      optionLabel: "---Chọn---",
      dataSource: {
        serverFiltering: true,
        transport: {
          read: {
            type: "POST",
            url: RestEndpoint.BASE_SERVICE_URL + "commonServiceRest/getDept",
            contentType: "application/json; charset=utf-8",
            dataType: "json",
          },
          parameterMap: function (options, operation) {
            return JSON.stringify({});
          },
        },
      },
    };

    $("#role-Code").focusout(function(){
    	var rolecode= $("#role-Code").val();
    	var pattern= new RegExp("^(?!.*(%))^.{0,100}$");
    	var check1=pattern.test(rolecode);
		if(check1){
    		$("#val1").text("") ;
    	}
	})
	$("#role-Name").focusout(function(){
		var rolename= $("#role-Name").val();
		var pattern= new RegExp("^(?!.*(%))^.{0,100}$");
    	var check2=pattern.test(rolename);
		if(check2){
    		$("#val2").text("") ;
    	}
	})
	$("#-description").focusout(function(){
		var description= $("#-description").val();
    	var pattern= new RegExp("^(?!.*(%))^.{0,100}$");
    	var check1=pattern.test(description);
		if(check3){
    		$("#val3").text("") ;
    	}
	})
    
    vm.doSearch = doSearch;
    function doSearch() {
    	var rolecode= $("#role-Code").val();
    	var rolename= $("#role-Name").val();
    	var description= $("#-description").val();
    	var pattern= new RegExp("^(?!.*(%))^.{0,100}$");
    	var pattern2= new RegExp("^(?!.*(%))^.{0,100}$");
    	var check1=pattern.test(rolecode);
    	var check2=pattern.test(rolename);
    	var check3=pattern2.test(description);
    	
    	if(!check1){
    		$("#val1").text("Dữ liệu nhập phải nhỏ hơn 100 ký tự") ;
    	}
    	if(!check2){
    		$("#val2").text("Dữ liệu nhập phải nhỏ hơn 100 ký tự");
    	}
    	if(!check3){
    		$("#val3").text("Dữ liệu nhập phải nhỏ hơn 100 ký tự");
    	}
    	if(!check1||!check2||!check3){
    		console.log("check worked");
    		return;
    	}
    	$("#val1").text("");
    	$("#val2").text("");
    	$("#val3").text("");
    	
      vm.showDetail = false;
      var grid = vm.rolesManagementGrid;
      if (grid) {
        grid.dataSource.query({
          page: 1,
          pageSize: 10,
        });
      }
    }

    function refreshGrid(d) {
      var grid = vm.rolesManagementFileGrid;
      if (grid) {
        grid.dataSource.data(d);
        grid.refresh();
      }
    }

    function fillDataTable(data) {
      vm.gridOptions = kendoConfig.getGridOptions({
        autoBind: true,
        sortable: false,
        resizable: true,
        columnMenu: false,
        toolbar: [
          {
            name: "actions",
            template:
              '<div class=" pull-left ">' +
              '<a class="btn btn-qlk padding-search-right addQLK"' +
              'ng-click="vm.add()" uib-tooltip="Thêm mới" translate><p class="action-button add" aria-hidden="true">Tạo mới</p></a>' +
              "</div>" +
              '<div class=" pull-left ">' +
              '<a class="btn btn-qlk padding-search-right lockQLAN"' +
              'ng-click="vm.lock()" uib-tooltip="Khóa" translate><p class="action-button lock" aria-hidden="true">Khóa</p></a>' +
              "</div>" +
              '<div class=" pull-left ">' +
              '<a class="btn btn-qlk padding-search-right unlockQLAN"' +
              'ng-click="vm.unlock()" uib-tooltip="Mở khóa" translate><p class="action-button unlock" aria-hidden="true">Mở khóa</p></a>' +
              "</div>" +
              '<div class="btn-group pull-right margin_top_button margin_right10">' +
              '<i data-toggle="dropdown" aria-expanded="false"><i class="fa fa-cog" aria-hidden="true"></i></i>' +
              '<div class="dropdown-menu hold-on-click dropdown-checkboxes" role="menu">' +
              '<label ng-repeat="column in vm.rolesManagementGrid.columns| filter: vm.gridColumnShowHideFilter">' +
              '<input type="checkbox" checked="column.hidden" ng-click="vm.showHideColumn(column)"> {{column.title}}' +
              "</label>" +
              "</div></div>",
          },
        ],
        dataBound: function (e) {},
        dataSource: {
          serverPaging: true,
          schema: {
        	  data: "data",
            total: function (response) {
              vm.count = response.total;
              return response.total;
            },
            data: function (response) {
              return response.data;
            },
          },
          transport: {
            read: {
              // Thuc hien viec goi service
              url: Constant.BASE_SERVICE_URL + "rolesService/roles/doSearch",
              contentType: "application/json; charset=utf-8",
              type: "POST",
            },
            parameterMap: function (options, type) {
            	vm.record=0;
              vm.rolesManagementSearch.page = options.page;
              vm.rolesManagementSearch.pageSize = options.pageSize;
              return JSON.stringify(vm.rolesManagementSearch);
            },
          },
          pageSize: 10,
        },
        noRecords: true,
        messages: {
          noRecords: gettextCatalog.getString("Không có kết quả hiển thị"),
        },
        pageable: {
          refresh: false,
          pageSizes: [10, 15, 20, 25],
          messages: {
            display: "{0}-{1} của {2} kết quả",
            itemsPerPage: "kết quả/trang",
            empty: "Không có kết quả hiển thị",
          },
        },

        columns: [
          {
            title:
              "<input type='checkbox' name='gridchkselectall' ng-click='vm.chkSelectAll($event);' ng-model='vm.chkAll' />",
            template:
              "<input type='checkbox' name='gridcheckbox' ng-click='vm.onCheck($event)' />",
            width: 35,
          },
          {
            title: gettextCatalog.getString("STT"),
            field: "stt",
            template: (dataItem) =>
              vm.rolesManagementGrid.dataSource.indexOf(dataItem) +
              1 +
              (vm.rolesManagementGrid.dataSource._page - 1) *
                vm.rolesManagementGrid.dataSource._pageSize,
            width: 40,
            headerAttributes: {
              style: "text-align:center; font-weight:Bold;",
            },
            attributes: {
              style: "text-align:center;",
            },
          },
          {
            title: "Mã nhóm quyền",
            field: "roleCode",
            width: 120,
            headerAttributes: {
              style: "text-align:center;font-weight:Bold;",
            },
            attributes: {
              style: "text-align:center;",
            },
          },
          {
            title: "Tên nhóm quyền",
            field: "roleName",
            width: 120,
            headerAttributes: {
              style: "text-align:center;font-weight:Bold;",
            },
            attributes: {
              style: "text-align:center;",
            },
          },
          {
            title: "Mô tả",
            field: "description",
            width: 120,
            headerAttributes: {
              style: "text-align:center;font-weight:Bold;",
            },
            attributes: {
              style: "text-align:center;",
            },
          },
          {
            title: "Trạng thái",
            field: "status",
            width: 90,
            template:
              "# if(status == 1){ #" +
              "#= 'Hoạt động' #" +
              "# } " +
              "else if (status == 0) { # " +
              "#= 'Không hoạt động' #" +
              "#} " +
              "#",
            headerAttributes: {
              style: "text-align:center;font-weight:Bold;",
            },
            attributes: {
              style: "text-align:center;",
            },
          },
          {
            title: "Thao tác nhóm quyền",
            template: (dataItem) =>
              '	<div class="text-center #=rolesManagementId#"">' +
              '		<a  type="button" class="#=rolesManagementId# icon_table" uib-tooltip="Thao tác nhóm quyền" translate> ' +
              '			<i class="fa fa-pencil-square-o" ng-click=vm.roleInteract(dataItem) style="color:blue;" aria-hidden="true"></i>' +
              "	</div>",
            width: 120,
            field: "actionss",
            headerAttributes: {
              style: "text-align:center;font-weight:Bold;",
            },
          },
          {
            title: "Thao tác",
            template: (dataItem) =>
              '	<div class="text-center #=rolesManagementId#"">' +
              '		<a  type="button" class="#=rolesManagementId# icon_table" uib-tooltip="Cập nhật nhóm quyền" translate> ' +
              '			<i class="fa fa-pencil" ng-click=vm.edit(dataItem) aria-hidden="true"></i>' +
              '		<a type="button" class="#=rolesManagementId# icon_table" uib-tooltip="Xóa" translate >' +
              '			<i class="fa fa-trash" ng-click=vm.remove(dataItem) aria-hidden="true" style="color:red;"></i>' +
              "		</a>" +
              "	</div>",
            width: 120,
            field: "actionss",
            headerAttributes: {
              style: "text-align:center;font-weight:Bold;",
            },
          },
        ],
      });
    }

    vm.add = function add() {
      vm.isCreateNew = true;
      vm.rolesManagementCreate = {};
      var teamplateUrl = "qlan/rolesManagement/rolesManagementPopup.html";
      var title = "Thêm mới nhóm quyền";
      var windowId = "rolesManagement";

      CommonService.populatePopupCreate(
        teamplateUrl,
        title,
        vm.rolesManagementCreate,
        vm,
        windowId,
        true,
        "80%",
        "60%"
      );
    };

    vm.edit = function edit(dataItem) {
      vm.isCreateNew = false;
      vm.rolesManagementCreate = dataItem;

      var templateUrl = "qlan/rolesManagement/rolesManagementPopup.html";
      var title = "Cập nhật nhóm quyền";
      var windowId = "rolesManagement";
      CommonService.populatePopupCreate(
        templateUrl,
        title,
        vm.rolesManagementCreate,
        vm,
        windowId,
        false,
        "80%",
        "60%"
      );
    };

    vm.roleInteract = function roleInteract(dataItem) {
      vm.rolesManagementCreate = dataItem;
      console.log(dataItem);
      console.log(vm.actionPopupGrid);
      var templateUrl = "qlan/rolesManagement/roleInteract.html";
      var title = "Thao tác nhóm quyền";
      var windowId = "objectsManagement";
  	  vm.objectSearch=null;	
	  vm.dataObject={};
	  vm.findObject={};
	  vm.object={};	
	  vm.record=0;
	  dataItem.pageSize=25000;
	  dataItem.page=1;
	  vm.dataObject.roleName = dataItem.roleName;
	  vm.dataObject.roleId=dataItem.roleId;
	  vm.dataObject.pageSize=25000;
	  vm.dataObject.page=1;
	  startLoading();
      rolesManagementService.getListObjectsById(dataItem).then(
			function(result) {
				stopLoading();
				 fillDataTableActionPopup(result);	
				 CommonService.populatePopupCreate(templateUrl,title,null,vm,windowId,false,'85%','80%');								 
			}, function(errResponse) {
				stopLoading();
				toastr.error("Lỗi !");
			});	
      
    };

    vm.cancel= function cancel(){
    	if(!vm.isCreateNew){
    		return;
    	}
    	doSearch();
    }
    vm.save = function save(isCreateNew) {
      startLoading();
      console.log("status : " + vm.isCreateNew);
      if (vm.isCreateNew) {
        if (
          vm.rolesManagementCreate.password == undefined ||
          vm.rolesManagementCreate.password == null
        ) {
          vm.rolesManagementCreate.password = "12345678";
        }
        rolesManagementService
          .createrolesManagement(vm.rolesManagementCreate)
          .then(
            function (result) {
              stopLoading();
              if (result.error) {
                toastr.error(result.error);
                return;
              }

              toastr.success("Thêm mới thành công!");
              
              var currentPage = vm.rolesManagementGrid.dataSource.page();
              vm.rolesManagementGrid.dataSource.page(currentPage);
              if(vm.isCreateNew){
            	  doSearch();
              }
//              doSearch();
//              $("#rolesManagementGrid").data("kendoGrid").dataSource.read();
//              $("#rolesManagementGrid").data("kendoGrid").refresh();
             CommonService.dismissPopup();
            },
            function (errResponse) {
              stopLoading();
              if (errResponse.data) {
                toastr.error(errResponse.data.errorMessage);
              } else {
                toastr.error(
                  gettextCatalog.getString(
                    "Lỗi xuất hiện khi tạo mới nhóm quyền!"
                  )
                );
              }
            }
          );
      } else {
        rolesManagementService
          .updaterolesManagement(vm.rolesManagementCreate)
          .then(
            function (result) {
              stopLoading();
              if (result.error) {
                toastr.error(result.error);
                return;
              }
              toastr.success("Cập nhật thành công!");
              var currentPage = vm.rolesManagementGrid.dataSource.page();
              vm.rolesManagementGrid.dataSource.page(currentPage);
              CommonService.dismissPopup();
            },
            function (errResponse) {
              stopLoading();
              if (errResponse.data) {
                toastr.error(errResponse.data.errorMessage);
              } else {
                toastr.error(
                  gettextCatalog.getString("Xảy ra lỗi khi cập nhật")
                );
              }
            }
          );
      }
    };

    vm.remove = function remove(data) {
      vm.rolesManagementCreate = data;
      confirm("Xác nhận xóa", function (d) {
        startLoading();
        rolesManagementService.remove(vm.rolesManagementCreate).then(
          function (result) {
        	  
            stopLoading();
            if (result.error) {
              toastr.error(result.error);
              return;
            }
            toastr.success("Xóa chức năng thành công!");
            var currentPage = vm.rolesManagementGrid.dataSource.page();
            var dataSize = vm.rolesManagementGrid.dataSource.data().length;
            if (currentPage > 1 && dataSize == 1) {
              vm.rolesManagementGrid.dataSource.page(currentPage - 1);
            } else {
              vm.rolesManagementGrid.dataSource.page(currentPage);
            }
          },
          function (errResponse) {
            stopLoading();
            if (errResponse.data) {
              toastr.error(errResponse.data.errorMessage);
            } else {
              toastr.error(
                gettextCatalog.getString("Lỗi khi xóa nhóm quyền")
              );
            }
          }
        );
      });
    };
    vm.lock = function () {
      var selectedRow = [];
      var grid = vm.rolesManagementGrid;
      grid.table.find("tr").each(function (idx, item) {
        var row = $(item);
        var checkbox = $('[name="gridcheckbox"]', row);
        if (checkbox.is(":checked")) {
          var dataItem = grid.dataItem(item);
          selectedRow.push(dataItem);
        }
      });
      if (selectedRow.length == 0) {
        toastr.warning("Bạn chưa chọn bản ghi !");
      } else {
        confirm("Xác nhận khóa", function (d) {
          startLoading();
          rolesManagementService.lock(selectedRow).then(
            function (result) {
              stopLoading();
              toastr.success("khóa chức năng thành công!");
              $("#rolesManagementGrid").data("kendoGrid").dataSource.read();
              $("#rolesManagementGrid").data("kendoGrid").refresh();
            },
            function (errResponse) {
              stopLoading();
              if (errResponse.data) {
                toastr.error(errResponse.data.errorMessage);
              } else {
                toastr.error(
                  gettextCatalog.getString("Xảy ra lỗi khi cập nhật trạng thái")
                );
              }
            }
          );
        });
      }
    };

    vm.unlock = function () {
      var selectedRow = [];
      var grid = vm.rolesManagementGrid;
      grid.table.find("tr").each(function (idx, item) {
        var row = $(item);
        var checkbox = $('[name="gridcheckbox"]', row);
        if (checkbox.is(":checked")) {
          var dataItem = grid.dataItem(item);
          selectedRow.push(dataItem);
        }
      });
      if (selectedRow.length == 0) {
        toastr.warning("Bạn chưa chọn bản ghi !");
      } else {
        confirm("Xác nhận mở khóa", function (d) {
          startLoading();
          rolesManagementService.unlock(selectedRow).then(
            function (result) {
              stopLoading();
              toastr.success("Mở khóa thành công!");
              $("#rolesManagementGrid").data("kendoGrid").dataSource.read();
              $("#rolesManagementGrid").data("kendoGrid").refresh();
            },
            function (errResponse) {
              stopLoading();
              if (errResponse.data) {
                toastr.error(errResponse.data.errorMessage);
              } else {
                toastr.error(
                  gettextCatalog.getString("Xảy ra lỗi khi cập nhật trạng thái")
                );
              }
            }
          );
        });
      }
    };

    vm.resetPass = function (dataItem) {
      confirm("Xác nhận reset password", function (d) {
        startLoading();
        rolesManagementService.resetPass(dataItem).then(
          function (result) {
            stopLoading();
            toastr.success("Reset mật khẩu thành công!");
            prompt("Mật khẩu được reset thành: " + result.password);
            return;
          },
          function (errResponse) {
            stopLoading();
            if (errResponse.data) {
              toastr.error(errResponse.data.errorMessage);
            } else {
              toastr.error(
                gettextCatalog.getString("Xảy ra lỗi khi cập nhật trạng thái")
              );
            }
          }
        );
      });
    };

    vm.showHideColumn = function (column) {
      if (angular.isUndefined(column.hidden)) {
        vm.rolesManagementGrid.hideColumn(column);
      } else if (column.hidden) {
        vm.rolesManagementGrid.showColumn(column);
      } else {
        vm.rolesManagementGrid.hideColumn(column);
      }
    };

    vm.gridColumnShowHideFilter = function (item) {
      return item.type == null || item.type != 1;
    };
    // handleCheck
    vm.onCheck = function (item) {
      if (document.getElementById("chkSelectAll").checked == true) {
        document.getElementById("chkSelectAll").checked = false;
      }
    };
    vm.chkSelectAll = function (item) {
      var grid = vm.rolesManagementGrid;
      chkSelectAllBase(vm.chkAll, grid);
    };
    vm.removeAll = function (lock) {
      var selectedRow = [];
      var grid = vm.rolesManagementGrid;
      grid.table.find("tr").each(function (idx, item) {
        var row = $(item);
        var checkbox = $('[name="gridcheckbox"]', row);
        if (checkbox.is(":checked")) {
          var dataItem = grid.dataItem(item);
          selectedRow.push(dataItem.rolesId);
        }
      });
      if (selectedRow.length == 0) {
        toastr.warning("Bạn chưa chọn bản ghi !");
      } else {
        confirm("Xác nhận xóa", function (d) {
          rolesManagementService.removeAll(selectedRow).then(
            function (d) {
              toastr.success("Xóa bản ghi thành công!");
              vm.doSearch();
            },
            function (errResponse) {
              toastr.error("Lỗi Xóa!");
            }
          );
        });
      }
    };
    vm.record = 0;

    function fillDataTableActionPopup(data) {
      vm.actionPopupGridOptions = kendoConfig.getGridOptions({
        autoBind: true,
        resizable: true,
        dataSource: data,
        noRecords: true,
        columnMenu: false,
        func: vm.record=0,
        
        messages: {
          noRecords: gettextCatalog.getString("Không có kết quả hiển thị"),
        },
        pageable: {
          refresh: true,
          pageSize: 10,
          pageSizes: [10, 15, 20, 25],
          messages: {
            display: "{0}-{1} của {2} kết quả",
            itemsPerPage: "kết quả/trang",
            empty: "Không có kết quả hiển thị",
          },
        },
        columns: [
          { 
            title: "STT",
            field: "stt",
            template: function(dataItem){
            	
              if(vm.actionPopupGrid != undefined ){
            	  if(vm.actionPopupGrid.dataSource.data().indexOf(dataItem)+1==0){
            		  
            		  return ++vm.record;
            	  }
                 return vm.actionPopupGrid.dataSource.data().indexOf(dataItem)+1;
                
                }else{
                	return ++vm.record;
                }
                }
                ,
            width: "5%",
            headerAttributes: {
              style: "text-align:center;",
            },
            attributes: {
              style: "text-align:center;",
            },
          },
          {
            title: "Mã chức năng",
            field: "objectCode",
            width: "20%",
            headerAttributes: {
              style: "text-align:center;",
            },
            attributes: {
              style: "text-align:left;",
            },
          },
          {
            title: "Tên chức năng",
            field: "objectName",
            width: "20%",
            headerAttributes: {
              style: "text-align:center;",
            },
            attributes: {
              style: "text-align:left;",
            },
          },
          {
            title: "Đường dẫn",
            field: "objectUrl",
            width: "20%",
            headerAttributes: {
              style: "text-align:center;",
            },
            attributes: {
              style: "text-align:left;",
            },
          },
          {
              title: "Thứ tự",
              field: "ord",
              width: "20%",
              headerAttributes: {
                style: "text-align:center;",
              },
              attributes: {
                style: "text-align:center;",
              },
            },
         {
        	  
                  title: "Loại chức năng",
                  field: "objectTypeId",
                  width: 90,
                  template:
                    "# if(objectTypeId == 1){ #" +
                    "#= 'Component' #" +
                    "# } " +
                    "else if (objectTypeId == 0) { # " +
                    "#= 'Module' #" +
                    "#} " +
                    "#",
                  headerAttributes: {
                    style: "text-align:center;font-weight:Bold;",
                  },
                  attributes: {
                    style: "text-align:center;",
                  }
          },
          {
            title: "Xóa",
            template: (dataItem) =>
              '<div class="text-center"">' +
              '<a type="button"' +
              'class="#=userRoleId# icon_table" style="color:red;"  uib-tooltip="Xóa" translate>' +
              '<i class="fa fa-times" aria-hidden="true" ng-click=caller.removeItem($event)></i>' +
              "</a>" +
              "</div>",
            width: "10%",
            field: "stt",
            headerAttributes: {
              style: "text-align:center;",
            },
            attributes: {
              style: "text-align:left;",
            },
          },
        ],
      });
    }
    vm.insertObjectData = function insertObjectData() {
    	console.log("glick");
      var listObjects = vm.actionPopupGrid.dataSource.data();
      vm.dataObject.listObject = listObjects;
      var objectIdList=[];
     listObjects.forEach( function(i){
    objectIdList.push(i.objectId);
     });
     vm.dataObject.objectIdList=objectIdList;
      console.log(vm.dataObject);
      startLoading();
      rolesManagementService.insertObjectData(vm.dataObject).then(
        function (result) {
          stopLoading();
          toastr.success("Thêm chức năng cho nhóm quyền thành công!");
          CommonService.dismissPopup();
        },
        function (errResponse) {
          stopLoading();
          if (errResponse.data) {
            toastr.error(errResponse.data.errorMessage);
          } else {
            toastr.error("Lỗi !");
          }
        }
      );
    };
    vm.headerTemplateStock =
      '<div class="dropdown-header row text-center k-widget k-header">' +
      '<p class="col-md-6 text-header-auto border-right-ccc">Mã chức năng</p>' +
      '<p class="col-md-6 text-header-auto">Tên chức năng</p>' +
      "</div>";
    vm.objectsOptions = {
      select: function (e) {
        var dataItem = this.dataItem(e.item.index());
        vm.object = dataItem; // thành id
        var grid = vm.actionPopupGrid;

        if (vm.object.objectCode != null && vm.object.objectName != "") {
          vm.objectSearch = null;

          grid.dataSource.data().push(vm.object);
          grid.refresh();
        }
        vm.objectSearch = null;
      },
      pageSize: 10,
      dataSource: {
        serverFiltering: true,
        transport: {
          read: {
            type: "POST",
            url:
              RestEndpoint.BASE_SERVICE_URL +
              "rolesService/roles/autoSearchObject",
            contentType: "application/json; charset=utf-8",
            dataType: "json"
            
          },
          parameterMap: function (options, type) {
            var listObjects = vm.actionPopupGrid.dataSource.data();
            var listId = [];
            for (var i = 0; i < listObjects.length; i++) {
              listId.push(listObjects[i].objectId);
            }
            var obj = {};
            obj.listId = listId;
            
            obj.objectName = vm.objectSearch.name;
           
           return JSON.stringify(obj);
           
          },
        },
      },
      template:
        '<div class="row" ><div class="col-xs-5" style="word-break: break-word;float:left">#: data.objectCode #</div><div  style="word-break: break-word;padding-left: 5px;width:auto;overflow: hidden"> #: data.objectName #</div> </div>',
      change: function (e) {
        if (e.sender.value() === "") {
          vm.object = null; // thành id
        }
      },
      ignoreCase: false,
    };

    
  }
})();
