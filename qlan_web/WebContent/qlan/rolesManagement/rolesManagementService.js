angular.module('MetronicApp').factory(
		'rolesManagementService',
		[
				'$http',
				'$q',
				'RestEndpoint',
				'Restangular',
				'$kWindow',
				function($http, $q, RestEndpoint, Restangular, $kWindow) {
					var serviceUrl = RestEndpoint.ROLES_URL;
					var factory = {
							createrolesManagement : createrolesManagement,
						remove : remove,
						updaterolesManagement : updaterolesManagement,
						doSearch : doSearch,
						getParent : getParent,
						lock: lock,
						unlock:unlock,
						resetPass:resetPass,
						getListRoleByUserId:getListRoleByUserId,
						insertUserRoleData:insertUserRoleData,
						getListObjectsById:getListObjectsById,
						autoSearchObject:autoSearchObject,
						insertObjectData:insertObjectData
					};

					return factory;

					function createrolesManagement(obj) {
						return Restangular.all(serviceUrl + "/roles/add").post(obj);
					}
					function remove(obj) {
						return Restangular.all(serviceUrl + "/roles/remove").post(obj);
					}

					function updaterolesManagement(obj) {
						return Restangular.all(serviceUrl + "/roles/update").post(obj);
					}

					function doSearch(obj) {
						console.log(Restangular.all(serviceUrl + "/roles/doSearch").post(obj));
						return Restangular.all(serviceUrl + "/roles/doSearch").post(obj);
					}
					function getParent() {
						return Restangular.all(serviceUrl + "/getParent").post();
					}
					function lock(obj) {
						return Restangular.all(serviceUrl + "/roles/lock").post(obj);
					}
					function unlock(obj) {
						return Restangular.all(serviceUrl + "/roles/unlock").post(obj);
					}
					function resetPass(obj) {
						return Restangular.all(serviceUrl + "/roles/resetPass").post(obj);
					}
					
					function getListRoleByUserId(id) {
						return Restangular.all(serviceUrl + "/roles/getListRoleByUserId").post(id);
					}
					
					function insertUserRoleData(obj) {
						return Restangular.all(serviceUrl + "/roles/insertUserRoleData").post(obj);
					}
					function getListObjectsById(obj){
						return Restangular.all(serviceUrl +"/roles/getListObjectsByRoleId").post(obj);
					}
					function autoSearchObject(obj){
						return Restangular.all(serviceUrl +"/roles/autoSearchObject").post(obj);
					}
					function insertObjectData(obj){
						return Restangular.all(serviceUrl+"/roles/insertObjectData").post(obj);
					}
				} ]);
